// This represents a CoLab Person(tm) registered trademark patent pending

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.ObjectId;

var PersonSchema = new Schema({
  name: String,
  codename: String,
  fbStaffId: Number,
  taxId: Number,
  birthday: Date,
  colabStartDate: Date,
  colabMembershipDate: Date,
  address: {
    street: String,
    street2: String,
    city: String,
    state: String,
    country: String,
    zipCode: String
  },
  phone: String,
  email: String,
  website: String,
  gmail: String,
  skype: String,
  github: String,
  bitbucket: String,
  hours: String,
  membershipStatus: String,
  meals: [{
    nameOfDish: String,
    cuisineOrigin: String,
    calories: Number
  }],
  notes: String
});

module.exports = process.app.connection.model('Person', PersonSchema);