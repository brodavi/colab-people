var mongoose = require('mongoose');
var Person = process.app.connection.model('Person');
var _ = require('highland');

module.exports = {
  create: function (req, res, next) {
    var person = new Person(req.body);
    person.save(function (err) {
      if (err) return res.jsonp(400, err);
      return res.jsonp(person);
    });
  },

  update: function (req, res, next) {
    var id = req.params.id;
    Person
      .findOne({'_id': id})
      .exec(function (err, person) {
        _.extend(req.body, person);
        person.save(function(err){
          if (err) {
            return res.send(err);
          }
          else {
            return res.status(200).json(person);
          }
        });
      });
  },

  all: function (req, res, next) {
    Person
      .find()
      .exec(function (err, persons) {
        if (err) return res.send(400, err);

        return res.json(persons);
      });
  },

  get: function (req, res, next) {
    var id = req.params.id;
    Person
      .findOne({_id: id})
      .exec(function (err, person) {
        if (err) return res.send(400, err);
        return res.json(person);
      });
  },

  addRoutes: function (app) {
    app.get('/persons', module.exports.all);
    app.get('/person/:id', module.exports.get);
    app.post('/persons', module.exports.create);
    app.put('/person/:id', module.exports.update);
  }
};