colab-persons
==============

A simple API for listing and creating/editing persons

clone the repo
==============
    $ git clone https://github.com/colab-coop/colab-persons.git

install deps (requires npm):
============================
    $ npm install

edit config file and copy
=========================
    $ cp config-dev.js config.js

run service
===========
    $ node app.js

the api
=======

### get all someModels
GET _/persons_

### get person
GET _/person/:personCode_

### create person
POST _/persons_ // with JSON payload matching model

### update person
PUT _/person/:personCode_ // with JSON payload matching model