'use strict';

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var config = require('./config').config;
var mongoose = require('mongoose');

process.app = {};

var db = mongoose.createConnection(config.db);
process.app.connection = db;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// CORS
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
  next();
});

var people = require('./modules/people');
process.app.people = people.models.people;

people.api.addRoutes(app);

var port = parseInt(process.env.PORT) || config.port;
app.listen(port);
console.log('somemodel service listening on port ' + port);