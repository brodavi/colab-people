#!/bin/bash

forever start -a \
    -l forever.log \
    -o out.log \
    -e error.log \
    --uid "colab-people" app.js
